module bitbucket.org/uwaploe/babcheck

go 1.15

require (
	bitbucket.org/uwaploe/bab v0.4.0
	github.com/gomodule/redigo v1.8.4
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	golang.org/x/sys v0.0.0-20210403161142-5e06dd20ab57 // indirect
)
