// Babcheck reads the battery status information from the battery aggregator
// board and writes the result to standard out in JSON format and stores the
// settings in a Redis database. The Redis format is compatible with that
// used by the old DP bcheck program.
package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"os"
	"runtime"
	"strconv"
	"time"

	"bitbucket.org/uwaploe/bab"
	"github.com/gomodule/redigo/redis"
	"github.com/tarm/serial"
)

const Usage = `Usage: babcheck [options]

Babcheck reads the battery status information from the battery aggregator
board, writes the result to standard out in JSON format and stores the
settings in a Redis database. The Redis format is compatible with that
used by the old DP bcheck program.

`

var Version = "dev"
var BuildDate = "unknown"

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
	rdAddr    string        = "localhost:6379"
	rdKey     string        = "battery_status"
	babDev    string        = "/dev/ttyAT6"
	babBaud   int           = 4800
	timeout   time.Duration = time.Second * 10
	wattHours float64       = 120
)

func redisStore(conn redis.Conn, name string, summary bab.Summary) error {
	t := time.Now()
	conn.Send("MULTI")
	conn.Send("HSET", name, "t", strconv.FormatInt(t.Unix(), 10))
	conn.Send("HSET", name, "count", strconv.Itoa(summary.Count))
	conn.Send("HSET", name, "temp", "0")
	conn.Send("HSET", name, "voltage", strconv.Itoa(summary.Voltage))
	conn.Send("HSET", name, "current", strconv.Itoa(summary.Amperage))
	energy := float64(summary.Count) * wattHours * float64(summary.Capacity) / 100
	conn.Send("HSET", name, "energy", strconv.Itoa(int(energy*1000)))
	conn.Send("HSET", name, "rel_charge", strconv.Itoa(summary.Capacity))
	conn.Send("HSET", name, "avg_current", strconv.Itoa(summary.Amperage))
	_, err := conn.Do("EXEC")

	return err
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.StringVar(&rdAddr, "addr", rdAddr, "Redis host:port")
	flag.StringVar(&rdKey, "key", rdKey, "Redis key for battery status")
	flag.StringVar(&babDev, "dev", babDev, "serial device for BAB")
	flag.IntVar(&babBaud, "baud", babBaud, "BAB baud rate")
	flag.DurationVar(&timeout, "timeout", timeout, "BAB read timeout")
	flag.Float64Var(&wattHours, "wh", wattHours, "watt-hour capacity per battery")
	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	_ = parseCmdLine()

	port, err := serial.OpenPort(&serial.Config{
		Name:        babDev,
		Baud:        babBaud,
		ReadTimeout: timeout,
	})
	if err != nil {
		log.Fatalf("Cannot open serial device %q: %v", babDev, err)
	}
	defer port.Close()

	dev := bab.New(port)
	summary, err := dev.GetAve()
	if err != nil {
		log.Fatal(err)
	}

	summary.Amperage = summary.Amperage * summary.Count
	json.NewEncoder(os.Stdout).Encode(summary)

	conn, err := redis.Dial("tcp", rdAddr)
	if err != nil {
		log.Fatalf("Cannot connect to Redis: %v", err)
	}
	defer conn.Close()

	redisStore(conn, rdKey, summary)
}
